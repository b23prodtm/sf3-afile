#!/usr/bin/env bash
[ -z $CIPHER_PWD ] && read -sp "Enter CIPHER_PWD:" CIPHER_PWD
[ -z $CI_DEPLOY_USERNAME ] && read -p "Enter CI_DEPLOY_USERNAME:" CI_DEPLOY_USERNAME
[ -z $CI_DEPLOY_PASSWORD ] && read -sp "Enter CI_DEPLOY_PASSWORD:" CI_DEPLOY_PASSWORD
[ -z $GPG_KEY ] && read -sp "Enter GPG_KEY (user@mail.com):" GPG_KEY
[ -z $GPG_PWD ] && read -sp "Enter GPG_PWD (press enter for none):" GPG_PWD
usage="$0 <-f, --force> <-e> <-m, --make-space>
Launches a Maven release:prepare and then try .circleci configuration YAML.
--force:
  Set docker daemon restart flag on
-e:
  Reset docker machine environment variables
--make-space:
  Remove exited containers to free some disk space"
while [[ "$#" -gt 0 ]]; do case $1 in
  -[fF]*|--force)
    docker-machine restart default;;
  -[eE]*)
    eval $(docker-machine env);;
  -[mM]*|--make-space)
    docker rm $(docker ps -q -f 'status=exited')
    docker volume rm $(docker volume ls -qf dangling=true);;
  -[hH]*|--help)
    echo -e "${usage}"
    exit 1;;
  *);;
esac; shift; done
mvn -V -B -s settings.xml release:prepare
circleci local execute || echo -e $usage


<!-- toc -->

- [Ant Build Files on Maven](#ant-build-files-on-maven)
  * [Project resources](#Project-resources)
  * [Build-Files.xml](#Build-Files.xml)
- [Configure and install](#Configure-and-install)
  * [Folder Certificates/](#Folder-Certificates/)
  * [The folder JREs/](#The-folder-JREs/)
  * [settings.xml](#settings.xml)
  * [Deploying a new release on (Nexus) Central Open Source Repository](#Deploying-a-new-release-on-Nexus-Central-Open-Source-Repository)
- [FAQ on build process or as child project :](#FAQ-on-build-process-or-as-child-project-:)
  * [An ERROR bad decrypt](#bad-decrypt)
  * [An ERROR occurs upon the above process until step 4.](#occurs-upon-the-above-process-until-step-4.)
  * [Under what license does this code distribute ?](#Under-what-license-does-this-code-distribute-?)
- [About OSSRH Central](#About-OSSRH-Central)

<!-- tocstop -->

Ant Build Files on Maven
------------------------
### Project resources
Located in src/main/resources, those files are shared between all SF3JSWING projects. They defines script properties and tasks for the Apache Ant tool.

### Build-Files.xml   
is the Apache Ant root build file, imported by any JXA project to run build ant task

Configure and install
---------------------

To change the original password required by the mvn process, you need to build your own `AntBuildFiles` package and please include your own code signing certificate.

1. Fork or clone your own copy of the source code from GIT :
> git clone git@bitbucket.org:b23prodtm/sf3-jxakn.git
2. browse to the stc/main/resources/Certificates/ folder and where you may
include you certificate files (caution : they may contain private keys). You should edit
3. set your personal password in settings.xml ([see below](#settings.xml)):
> CIPHER_PWD="yourpassword"
3. build an artifact from Maven build, run while in the resulted clone local folder :
> mvn install

### Folder Certificates/
If you wish to sign with a set of verified certificates, this folder needs additional files :

  `AppleIncRootCertificate.cer`
  `DeveloperID.p12`
  `DeveloperIDCA.cer`
  `developerID_application.cer`

You can get those files from various providers (file names will vary), or your developer.apple.com member center (purchased code signing certificates). Learn more about code signing certificates in the JWrapper [developer's documentation.](http://www.jwrapper.com/guide-code-signing.html)

  `devid.xml` (configured as of devid-sample.xml template)
  `my.keystore` (or any other name specified in devid.xml)

The keystore may be edited with the freeware KeyStore Explorer. It can import DeveloperID.p12 files as a key/pair. [https://sourceforge.net/projects/keystore-explorer]()
An unlimited Java Cryptographic Policy is necessary to import certificates. Please follow Keystore Explorer guidelines.

### The folder JREs/
must filled with the Java Runtime Environments of each of the 5 platforms :

  `linux/i586`
  `linux/x64`
  `macosx/x64`
  `windows/i586`
  ``windows/x64`

Please, extract all JRE.tarballs in their respective folders. e.g. macosx/x64/ ends up in jre<version>.jre folder.

Edit `build-jwrapper.xml` accordingly to your JRE versions. See line  `<filter token="java.version" value="<version>"/>`

### settings.xml
Usually found in `~USER/.m2/`, home hidden folder, this file is Maven user's configuration file. It can define a couple of variables, such as environment, profiles and servers credentials, that must be kept secret from the public.

As a result, there are a few variables to set up for SF3JSWING projects management.
Here's a sample of the settings file used locally :
```
# settings.xml
<!-- Please, refer to http://central.sonatype.org/pages/apache-maven.html for more information. -->
<settings>
  <profiles>
    <profile>
      <id>-config</id>
      <activation>
        <activeByDefault>true</activeByDefault>
      </activation>
      <properties>
        <env.CIPHER_PWD>enCrypTed</env.CIPHER_PWD>
      </properties>
    </profile>
  </profiles>
</settings>
```
About OSSRH Central
-------------------

If you wish to deploy your open source code and binaries to the Maven central repository, the process is fairly straight forward. There are only a few steps to complete :

- to configure a GPG identity on your computer station, e.g. with GNU GPG :
  1. basically, you create a personal key/pair Certificate (name and email);
  2. you publish it to a public Directory Service on the web.
- to understand to meaning and purposal of managing a project with JIRA. A project's essentially a bunch of issues...

All information is available at [Sonatype.org](http://central.sonatype.org/pages/ossrh-guide.html)

Deploying to the central repo with Maven projects : `~USER/.m2/settings.xml` must be filled with your JIRA subscription.
You must add or modifiy a profile "ossrh-release" that activates itself when you run "mvn clean deploy".
```
<!-- Please, refer to http://central.sonatype.org/pages/apache-maven.html for more information. -->
<servers>
  <server>
      <id>ossrh</id>
      <username>your-JIRA-id</username>
      <password>your-JIRA-password</password>
  </server>
</servers>
<profiles>
  <profile>
      <id>ossrh-release</id>
      <activation>
        <activeByDefault>true</activeByDefault>
      </activation>
      <properties>
        <gpg.executable>/usr/local/bin/gpg</gpg.executable>
        <gpg.passphrase>gpg-password</gpg.passphrase>              
        </properties>
  </profile>
</profiles>
```
### Deploying a new release on Nexus Central Open Source Repository

1. Set to a -SNAPSHOT version of your project
(Netbeans : project > Custom > Reset Version)
> mvn versions:set -DnewVersion=1.2.3-SNAPSHOT
2. Commit (git commit) changes to your local Git repository. Synchronization with the remote SCM (git push) is done by the 4th step, upon deploying release.
(Netbeans : project > Git > Commit...). Nevertheless, you need to be sure that nothing is about to conflict with remote state. `git status` must return 'up-to-date'.
3. (optional) Perform a -SNAPSHOT release on [Sonatype's Central Repository](http://central.sonatype.org/pages/apache-maven.html#performing-a-snapshot-deployment)
(Netbeans : project > Custom > Deploy)
> mvn clean deploy
4. Prepare a release (Git push)
(Netbeans : project > Custom > Prepare release)
> mvn release:clean release:prepare
5. Perform a release to publish on [Sonatype's Central Repository](http://central.sonatype.org/pages/apache-maven.html#performing-a-release-deployment) (source, javadoc, binaries with GPG signatures are required)
(Netbeans : project > Custom > Perform release)
> mvn release:perform

### Validate a Continuous integration build on CircleCI

Launches a Maven release:prepare and then try .circleci configuration YAML.

Docker CE deamon-machine must be installed and running. The below script will try to connect to it locally.
Run the build script from project folder, it will ask for at least 3 variables, linked to a [Sonatype Nexus account](sonatype.com/nexus-repository-oss) (it's an Open source repository for Maven projects) :
> .circleci/build.sh

It should prepare the Maven project for release, and then a continuous integration release will occur after the next git push.

For more information about this build script :

> .circleci/build.sh --help


FAQ on build process or as child project :
------------------------------------------

### occurs upon the above process until step 4.
make some modifications to resolve the error, and you should proceed to step 1. again. The release goals can be found in the nbactions.xml file of the root folder. You might need to run "git commit -a" locally before to release.

### bad decrypt
> 2940:error:06065064:digital envelope routines:EVP\_DecryptFinal\_ex:bad decrypt:/SourceCache/OpenSSL098/OpenSSL098-51.2/src/crypto/evp/evp_enc.c:330:
[ERROR] Command execution failed.

This error was caused by an undefined environment variable `${env.CIPHER\_PWD}`, because those files were packed and crypted by a maven builder. The dependency [kernelapi] contains crypted certificate files, named `AntBuildFiles`. More about this, [here][kernelapi].

### Under what license does this code distribute ?
```
(c) 2016 - b23:production GNU/GPL

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```
[kernelapi]:http://www.sourceforge.net/projects/sf3jswing "Kernel-core"
